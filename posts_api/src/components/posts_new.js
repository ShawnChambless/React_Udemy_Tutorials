import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';
import { createPost } from '../actions';
import { connect } from 'react-redux';

class PostsNew extends Component {
	
	renderField(field) {
		const { meta: { touched, error } } = field;
		const className                    = `form-group ${touched && error ? 'has-danger' : ''}`;
		return (
				<div className={ className }>
					<label>{ field.label }</label>
					<input
							className='form-control'
							type='text'
							{ ...field.input }
					/>
					<div className='text-help'>
						{ touched && error ? error : '' }
					</div>
				</div>
		);
	}
	
	onSubmit(values) {
		this.props.createPost(values, () => this.props.history.push('/'));
	}
	
	render() {
		const { handleSubmit } = this.props;
		return (
				<form name='PostsNewForm' onSubmit={ handleSubmit(this.onSubmit.bind(this)) }>
					<Field name='title' label='Title' component={ this.renderField }/>
					<Field name='categories' label='Categories' component={ this.renderField }/>
					<Field name='content' label='Post Content' component={ this.renderField }/>
					<button type='submit' className='btn btn-primary'>Submit</button>
					<Link to='/' className='btn btn-danger' style={ { marginLeft: '5px' } }>Cancel</Link>
				</form>
		);
	}
}

const validate = values => {
	const errors = {};
	
	if(!values.title) errors.title = 'Enter a title';
	if(!values.categories) errors.categories = 'Enter some categories';
	if(!values.content) errors.content = 'Enter some content';
	
	return errors;
};

export default reduxForm({
	form: 'PostsNewForm'
	, validate
})(
		connect(null, { createPost })(PostsNew)
);