import React, { Component } from 'react';
import { connect } from 'react-redux';
import { deletePost, getSpecificPost } from '../actions';
import { Link } from 'react-router-dom';

class PostSingle extends Component {
	
	componentDidMount() {
		const { match: { params: { id } } } = this.props;
		this.props.getSpecificPost(id);
	}
	
	onDeleteClick(id) {
		this.props.deletePost(id, () => this.props.history.push('/'));
	}
	
	render() {
		const { post } = this.props;
		if(!post) return <div>Loading...</div>;
		return (
				<div>
					<button className='btn btn-danger float-right'
									onClick={ this.onDeleteClick.bind(this, post.id) }>
						Delete
					</button>
					<Link to='/'>{ `< Back to Posts` }</Link>
					<h3>{ post.title }</h3>
					<h6>Categories: { post.categories }</h6>
					<p>{ post.content }</p>
				</div>
		);
	}
}

const mapStateToProps = ({ posts }, ownProps) => {
	return { post: posts[ ownProps.match.params.id ] };
};

export default connect(mapStateToProps, { getSpecificPost })(
		connect(null, { deletePost })(PostSingle));