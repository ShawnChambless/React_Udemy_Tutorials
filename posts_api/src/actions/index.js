import axios from 'axios';

export const FETCH_POSTS       = 'FETCH_POSTS';
export const CREATE_POST       = 'CREATE_POST';
export const GET_SPECIFIC_POST = 'GET_SPECIFIC_POST';
export const DELETE_POST       = 'DELETE_POST';
const ROOT_URL                 = 'http://reduxblog.herokuapp.com/api';
const KEY                      = '?key=FINE49821';

export const fetchPosts = () => {
	const request = axios.get(`${ROOT_URL}/posts${KEY}`);
	
	return {
		type:      FETCH_POSTS
		, payload: request
	};
};

export const createPost = (post, cb) => {
	const url     = `${ROOT_URL}/posts${KEY}`;
	const request = axios.post(url, post)
			.then(cb);
	
	return {
		type:      CREATE_POST
		, payload: request
	};
};

export const getSpecificPost = id => {
	const url     = `${ROOT_URL}/posts/${id}/${KEY}`;
	const request = axios.get(url);
	
	return {
		type:      GET_SPECIFIC_POST
		, payload: request
	};
};

export const deletePost = (id, cb) => {
	const url     = `${ROOT_URL}/posts/${id}/${KEY}`;
	const request = axios.delete(url)
			.then(cb);
	
	return {
		type:      DELETE_POST
		, payload: request
	};
};
