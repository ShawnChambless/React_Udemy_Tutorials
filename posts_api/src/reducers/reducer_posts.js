import { CREATE_POST, DELETE_POST, FETCH_POSTS, GET_SPECIFIC_POST } from '../actions';
import _ from 'lodash';

export default function(state = {}, action) {
	switch(action.type) {
		case FETCH_POSTS:
			return _.keyBy(action.payload.data, 'id') || {};
		case GET_SPECIFIC_POST:
			return { ...state, [ action.payload.data.id ]: action.payload.data };
		case CREATE_POST:
		case DELETE_POST:
			return _.omit(state, action.payload);
		default:
			return state;
	}
}