import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import registerServiceWorker from './registerServiceWorker';
import { Provider } from 'react-redux';
import { applyMiddleware, createStore } from 'redux';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import reducers from './reducers';
import promise from 'redux-promise';
import PostsIndex from './components/posts_index';
import PostsNew from './components/posts_new';
import PostSingle from './components/post_single';

const createStoreWithMiddleware = applyMiddleware(promise)(createStore);

ReactDOM.render(
		<Provider store={ createStoreWithMiddleware(reducers) }>
			<BrowserRouter>
				<div>
					<Switch>
						<Route path='/posts/single/:id' component={ PostSingle }/>
						<Route path="/posts/new" component={ PostsNew }/>
						<Route path="/" component={ PostsIndex }/>
					</Switch>
				</div>
			</BrowserRouter>
		</Provider>
		, document.getElementById('root'));
registerServiceWorker();
