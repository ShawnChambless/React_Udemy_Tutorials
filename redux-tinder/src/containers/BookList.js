import React, { Component } from 'react';
import { connect } from 'react-redux';
import { selectBook } from '../actions';
import { bindActionCreators } from 'redux';

class BookList extends Component {
	renderList() {
		return this.props.books.map((book) => <li className='list-group-item'
																							onClick={ () => this.props.selectBook(book) }
																							key={ book.title }>{ book.title }</li>);
	}
	
	render() {
		return (
				<ul className='list-group col-sm-4'>
					{ this.renderList() }
				</ul>
		);
	}
}

/**
 * Anything returned from this function will end up as props on the BookList container
 */
const mapStateToProps = (state) => {
	return {
		books: state.books
	};
};

const mapDispatchToProps = (action) => {
	/**
	 * Whenever selectBook is called, the result should be passed to all of the reducers
	 */
	return bindActionCreators({ selectBook: selectBook }, action);
};

/**
 * Promote BookList from component to container
 * It needs to know about this new dispatch method, selectBook. Make it available
 * as a prop.
 */
export default connect(mapStateToProps, mapDispatchToProps)(BookList);