import React, { Component } from 'react';
import CardHeader from './CardHeader';
import CardBody from './CardBody';
import CardButtons from './CardButtons';
import image from './../assets/main.jpg';


function Card() {
	return (
		<div style={{width: '500px'}}>
			<div style={{ display: 'flex' }}>
				<div className='imgBox'>
					<img className='img' src={image} height={50} width={50} alt={'Logo'} />
				</div>
				<CardHeader header={{ title: 'The Practical Dev', body: 'Learning React? Start Small.', mention: 'ThePracticalDev', date: 'Sep 10', author: '@dceddia' }} />
			</div>
			<CardBody />
			<CardButtons />
		</div>
	);
}

export default Card;

