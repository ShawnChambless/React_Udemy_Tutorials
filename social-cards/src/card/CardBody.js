import React, { Component } from 'react';

class CardBody extends Component {
	render() {
		return (
			<div className="card-body" style={{ position: 'relative' }}>
				<div style={{ width: '80%'}} >
					<h1 className="card-body-text">Learning React? Start Small.</h1>
				</div>
			</div>
		);
	}
}

export default CardBody;