import React from 'react';
import './../App.css';

function CardHeader(props) {


	return (
		<div>
			<header>
				<span style={{ display: 'flex' }}>
					<div className={'card-header-font'} style={{ fontWeight: '500' }}>{props.header.title}</div>
					<div className={'card-header-font-light'}>&nbsp;@{props.header.mention} <span style={{ fontSize: '5px' }}>•</span> {props.header.date}</div>
				</span>
				<div className={'card-header-font'}>
					{props.header.body}
					<br />
					{"{ author: " + props.header.author + " }"}
				</div>
			</header>
		</div>
	);
}

export default CardHeader;