import axios from 'axios';

const API_KEY              = 'f4e3decf1dfcca9ee8d6e3a451be5bb2';
const ROOT_URL             = `http://api.openweathermap.org/data/2.5/forecast?appid=${API_KEY}&units=imperial`;
export const FETCH_WEATHER = 'FETCH_WEATHER';

export const fetchWeather = (city) => {
	const url     = `${ROOT_URL}&q=${city},us`;
	const request = axios.get(url);
	
	return {
		type:      FETCH_WEATHER
		, payload: request
	};
};