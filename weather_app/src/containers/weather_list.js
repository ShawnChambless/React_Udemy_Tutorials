import React, { Component } from 'react';
import { connect } from 'react-redux';
import Chart from '../components/chart';
import GoogleMap from '../components/google_maps';

class WeatherList extends Component {
	renderWeather(city, ind) {
		const temps        = city.list.map(a => a.main.temp);
		const pressures    = city.list.map(a => a.main.pressure);
		const humidity     = city.list.map(a => a.main.humidity);
		const { lat, lon } = city.city.coord;
		console.log(lat, lon, city.city.name);
		return (
				<tr key={ ind }>
					<td>
						{ <GoogleMap lat={ lat } lon={ lon }/> }
					</td>
					<Chart data={ [ ...temps ] } units="F" color="yellow"/>
					<Chart data={ [ ...pressures ] } units="hPa" color="grey"/>
					<Chart data={ [ ...humidity ] } units="%" color="green"/>
				</tr>
		);
	}
	
	render() {
		console.log(this.props.weather);
		return (
				<table className='table table-hover'>
					<thead>
					<tr>
						<th>City</th>
						<th>Temperature (F)</th>
						<th>Pressure (hPa)</th>
						<th>Humidity (%)</th>
					</tr>
					</thead>
					<tbody>
					{ this.props.weather.map(this.renderWeather) }
					</tbody>
				</table>
		);
	}
	
}

const mapStateToProps = ({ weather }) => {
	return { weather };
};

export default connect(mapStateToProps)(WeatherList);