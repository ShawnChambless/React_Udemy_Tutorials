import React from 'react';
import { Sparklines, SparklinesLine, SparklinesReferenceLine } from 'react-sparklines';

const average = (data) => {
	return Math.ceil(data.reduce((a, b) => a + b) / data.length);
};

const Chart = (props) => {
	return (
			<td>
				<Sparklines height={ 100 } width={ 180 } data={ props.data }>
					<SparklinesLine color={ props.color }/>
					<SparklinesReferenceLine type="avg"/>
				</Sparklines>
				<div>{ average(props.data) } { props.units }</div>
			</td>
	);
};

export default Chart;